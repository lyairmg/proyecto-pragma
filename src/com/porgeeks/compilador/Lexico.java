/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.porgeeks.compilador;

import java.util.Arrays;

/**
 * @version 0.1v
 * @author lyairmg
 */
public class Lexico {

    private final String entrada;
    private int i = 0;
    private CLexico cl;
    private String valor = "", tipo = "";
    private final String[] pal_rese = {"int", "for", "float", "public", "main"};
    private final String[] pal_arit = {"+", "-", "*", "/"};
    private final String[] pal_rela = {"<", ">", "==", "<=", ">="};
    private final String[] pal_espe = {",", ";", "(", ")", "=", "{", "}", "[", "]", "'"};

    public Lexico(String entrada) {
        this.entrada = entrada;
    }

    public int getI() {
        return i;
    }

    private boolean isDigit(char s) {
        return Character.isDigit(s);
    }

    private boolean isDiorLe(char s) {
        return Character.isLetterOrDigit(s);
    }

    private boolean isPalRese(String s) {
        return Arrays.toString(pal_rese).contains(s);
    }

    private boolean isSimArit(String s) {
        return Arrays.toString(pal_arit).contains(s);
    }

    private boolean isSimRela(String s) {
        return Arrays.toString(pal_rela).contains(s);
    }

    private boolean isSimEspe(String s) {
        return Arrays.toString(pal_espe).contains(s);
    }

    public CLexico lex() {
        while (i < entrada.length()) {
            char caracter = entrada.charAt(i);
            if (isDiorLe(caracter)) {
                if (isDigit(caracter)) {
                    //System.out.println(i);
                    valor = String.valueOf(entrada.charAt(i));
                    int j = i + 1;
                    while (isDigit(entrada.charAt(j))) {
                        valor += entrada.charAt(j);
                        j++;
                        if (j == entrada.length()) {
                            break;
                        }
                    }
                    tipo = "Numerico";
                    i = j;
                    i++;
                    break;
                } else {
                    //System.out.println(i);
                    valor = String.valueOf(entrada.charAt(i));
                    int j = i + 1;
                    while (isDiorLe(entrada.charAt(j))) {
                        valor += entrada.charAt(j);
                        j++;
                        if (j == entrada.length()) {
                            break;
                        }
                    }
                    tipo = "iD";
                    i = j;
                    i++;
                    // Comparar si es una palabra reservada
                    if (isPalRese(valor)) {
                        tipo = "Reservada";
                    }
                    break;
                }
            } else if (isSimRela(String.valueOf(caracter))) {
                //System.out.println(i);
                valor = String.valueOf(entrada.charAt(i));
                int j = i + 1;
                while (isSimRela(String.valueOf(entrada.charAt(j)))) {
                    valor += entrada.charAt(j);
                    j++;
                    if (j == entrada.length()) {
                        break;
                    }
                }
                tipo = "Relacion";
                i = j;
                i++;
                break;
            } else if (isSimEspe(String.valueOf(caracter))) {
                //System.out.println(i);
                tipo = "Especial";
                valor = String.valueOf(caracter);
                i++;
                break;
            } else if (isSimArit(String.valueOf(caracter))) {
                //System.out.println(i);
                tipo = "Aritmetico";
                valor = String.valueOf(caracter);
                i++;
                break;
            } else {
                //System.out.println(i);
                tipo = "SN";
                valor = String.valueOf(caracter);
                i++;
                break;
            }
        }
        return cl = new CLexico(tipo, valor);
    }
}
