/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.porgeeks.compilador;

/**
 * @version 0.1v
 * @author lyairmg
 */
public class CLexico {
    private String expresion;
    private String token;

    public CLexico(String expresion, String token) {
        this.expresion = expresion;
        this.token = token;
    }

    public String getExpresion() {
        return expresion;
    }

    public void setExpresion(String expresion) {
        this.expresion = expresion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "CLexico{" + "expresion=" + expresion + ", token=" + token + '}';
    }
}
