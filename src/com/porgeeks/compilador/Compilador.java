/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.porgeeks.compilador;

import java.util.Arrays;

/**
 * @version 0.1v
 * @author lyairmg
 */
public class Compilador {

    private Lexico lexico;

    /**
     *
     * @param s
     */
    public Compilador(String[] s) {
        lexico = new Lexico(Arrays.toString(s));
        while (lexico.getI() < Arrays.toString(s).length()) {
            CLexico c = lexico.lex();
            System.out.println(c.toString()); 
        }
    }
    
    public Compilador(String s) {
        lexico = new Lexico(s);
        while (lexico.getI() < s.length()) {
            CLexico c = lexico.lex();
//            System.out.println(s.length()+" - "+ lexico.getI());
            System.out.println(c.toString()); 
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Compilador compilador = new Compilador("int x=20; asss");
    }
}
